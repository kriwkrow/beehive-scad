wallThickness = 4;
holeDiameter = 5;
holeSpacing = 10;
holeFN = 45;

Attachment55x55x35_numHolesX = 5;
Attachment55x55x35_numHolesY = 4;
Attachment55x55x35_numHolesZ = 3;

Beam_size = 20;
Beam_holeSpacing = 12;
Beam_headLength = 30;

Interposer_numHolesX = 3;
Interposer_numHolesZ = 3;