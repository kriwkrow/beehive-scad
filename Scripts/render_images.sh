#!/bin/bash

IMG_DIR="images"
PARTS_DIR="Parts"
COMBS_DIR="Combinations"

# Set up images directory
cd ..
if [ -d "${IMG_DIR}" ];
then
    rm -rf "${IMG_DIR}"
fi

mkdir "${IMG_DIR}"
cd "${IMG_DIR}"

# Render parts

for file in ../${PARTS_DIR}/*.scad
do 
    echo "${file}"
    name=$(basename -a --suffix=.scad ${file})
    echo "${name}" 
    openscad -o "${name}.png" --imgsize=800,600 --viewall --autocenter "${file}"
done

# Render combinations

for file in ../${COMBS_DIR}/*.scad
do 
    echo "${file}"
    name=$(basename -a --suffix=.scad ${file})
    echo "${name}" 
    openscad -o "${name}.png" --imgsize=800,600 --viewall --autocenter "${file}"
done

