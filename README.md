# Beehive SCAD

An OpenSCAD version of the Beehive project by Quentin Bolsee.

![Hero Shot](https://gitlab.cba.mit.edu/quentinbolsee/beehive-axes/-/raw/main/examples/linear_axis/img/axis.jpg)
Image &copy; Quentin Bolsee

As said on the [Beehive project repository](https://gitlab.cba.mit.edu/quentinbolsee/beehive-axes):

> This project provides a series of modular parts that can be easily composed to make linear motion axes and machine components. 

## Terminology

There are several categories of **Parts** and there can be **Combinations** between them. 

> **Note:** Edges of the elements should align with the center of the next (invisible) hole. Thus in combinations we would only loose one possible hole, no more.

### Beam

- 20x20Beam

### Endcaps

#### EndcapShort
![EndcapShort](https://kriwkrow.gitlab.io/beehive-scad/images/EndcapShort.png)

- Endcap
- Endcap2
- Endcap2Short
- Endcap3
- Endcap3Short

### Attachments

#### Attachment55x55x35
![Attachment55x55x35](https://kriwkrow.gitlab.io/beehive-scad/images/Attachment55x55x35.png)

- MotorWithCapstan

### Interposers

#### Universal Interposer
![Universal Interposer](https://kriwkrow.gitlab.io/beehive-scad/images/Interposer.png?v=2023-01-28b)

### Carriage

- CarriageBase
- CarriageString

### Combinations

#### Attachment55x55x35_EndcapShort
![Attachment55x55x35_EndcapShort](https://kriwkrow.gitlab.io/beehive-scad/images/Attachment55x55x35_EndcapShort.png)

- MotorWithCapstan_EndcapShort

## Customize

To be continued...

## Files

To be continued...


