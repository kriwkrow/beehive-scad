include <../Variables.scad>

module EndcapShort(){
  difference(){
    EndcapShort_Outer();
    
    // Adding 1/2 wall thickness more here to compensate additional size
    innerTranslateX = wallThickness; 
    innerTranslateY = 0;
    innerTranslateZ = wallThickness;
    translate([innerTranslateX, innerTranslateY, innerTranslateZ])
      EndcapShort_Inner();
    
    // Drill holes along Y axis
    EndcapShort_DrillY();
    
    // Drill holes along Z axis
    EndcapShort_DrillZ();
  }
}

module EndcapShort_Outer(){
  outerSizeX = Beam_headLength + wallThickness;
  outerSizeY = Beam_size + (wallThickness * 2);
  outerSizeZ = Beam_size + wallThickness; 
  cube([outerSizeX, outerSizeY, outerSizeZ], center=true);
}

module EndcapShort_Inner(){
  // Add wall thickness here to help cutting later
  innerSizeX = Beam_headLength + wallThickness; 
  
  // No additional wall thickness here as we do not move along Y axis
  innerSizeY = Beam_size;
  
  // Add wall thickness also here to help cutting later 
  innerSizeZ = Beam_size + wallThickness;
  
  cube([innerSizeX, innerSizeY, innerSizeZ], center=true);
}

module EndcapShort_DrillY(){
  // First translation offsets drill so it is relative to pocket center
  drillYOffsetX = wallThickness / 2;
  drillYTranslateX = (Beam_holeSpacing / 2);
  // Add one wall thickness to length to help cutting
  drillYLength = Beam_size + (wallThickness * 2) + wallThickness;
  translate([drillYOffsetX, 0, 0])
    translate([drillYTranslateX, 0, wallThickness / 2])
      rotate([90, 0, 0])
        cylinder(h = drillYLength, d = holeDiameter, $fn = holeFN, center=true);
}

module EndcapShort_DrillZ(){
  drillZOffsetX = wallThickness / 2;
  // Add one extra wall thickness to drill length to help cutting
  drillZLength = Beam_size + wallThickness + wallThickness;
  drillZTranslateX = -(Beam_holeSpacing / 2);
  translate([drillZOffsetX, 0, 0])
    translate([drillZTranslateX, 0, 0])
      cylinder(h = drillZLength, d = holeDiameter, $fn = holeFN, center=true);
}

EndcapShort();

