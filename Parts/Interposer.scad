include <../Variables.scad>

module Interposer(){
  union(){
    difference(){
      Interposer_HorizontalPlane();
      Interposer_HorizontalPlane_Holes();
    }
  
    difference(){
      Interposer_VerticalPlane();
      Interposer_VerticalPlane_Holes();
    }
  
    Interposer_Reinforcement();
  }
}

module Interposer_HorizontalPlane(){
  HP_sizeX = (Interposer_numHolesX + 1) * holeSpacing;
  HP_sizeY = holeSpacing * 2;
  HP_sizeZ = wallThickness;
  
  translate([0, -HP_sizeY/2, 0])
    cube([HP_sizeX, HP_sizeY, HP_sizeZ]); 
}

module Interposer_HorizontalPlane_Holes(){
  for(idx = [1:Interposer_numHolesX + 1]){
    // We bring the center of the -1nth hole to align with the outer wall
    translate([idx * holeSpacing , 0, wallThickness/2])
      cylinder(d=holeDiameter, h=wallThickness*2, center=true, $fn=holeFN);
  }
}

module Interposer_VerticalPlane(){
  VP_sizeX = wallThickness;
  VP_sizeY = holeSpacing * 2;
  VP_sizeZ = (Interposer_numHolesX + 1) * holeSpacing;
  
  translate([0, -VP_sizeY/2, 0])
    cube([VP_sizeX, VP_sizeY, VP_sizeZ]); 
}

module Interposer_VerticalPlane_Holes(){
  rotate([0, -90, 0]){
    for(idx = [1:Interposer_numHolesX + 1]){
      // We bring the center of the -1nth hole to align with the outer wall
      translate([idx * holeSpacing , 0, -wallThickness/2])
        cylinder(d=holeDiameter, h=wallThickness*2, center=true, $fn=holeFN);
    }
  }
}

module Interposer_Reinforcement(){
  R_sizeX = ((Interposer_numHolesX) * holeSpacing);
  R_sizeZ = ((Interposer_numHolesZ) * holeSpacing);
  R_sizeY = wallThickness / 1.5;
  R_translateY = holeSpacing;
  
  translate([0, R_translateY, 0])
    rotate([90, 0, 0])
      linear_extrude(R_sizeY)
        polygon(points = [[wallThickness,wallThickness], [wallThickness,R_sizeZ], [R_sizeX,wallThickness]]);
  
  translate([0, -R_translateY + R_sizeY, 0])
    rotate([90, 0, 0])
      linear_extrude(R_sizeY)
        polygon(points = [[wallThickness,wallThickness], [wallThickness,R_sizeZ], [R_sizeX,wallThickness]]);
}

Interposer();

