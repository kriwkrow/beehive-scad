include <../Variables.scad>

module Attachment55x55x35(){
  difference(){
    Attachment55x55x35_BoxOuter();
    
    // Cutouts
    Attachment55x55x35_BoxInner();
    
    // Cut left side holes. 
    // We need only those here, because the right side is going to be connected.
    holesX_translateX = (Attachment55x55x35_numHolesX * holeSpacing) / 2;
    translate([-holesX_translateX, 0, 0])
      Attachment55x55x35_DrillX();
    
    // Cut front side holes
    holesY_translateY = (Attachment55x55x35_numHolesY * holeSpacing) / 2; 
    translate([0, -holesY_translateY, 0])
      Attachment55x55x35_DrillY();
    
    // Cut back side holes
    translate([0, holesY_translateY, 0])
      Attachment55x55x35_DrillY();
    
    holesZ_translateZ = (Attachment55x55x35_numHolesZ * holeSpacing) / 2;
    translate([0, 0, -holesZ_translateZ])
      Attachment55x55x35_DrillZ();
  }
}

// This is the outer boundary of the box.
module Attachment55x55x35_BoxOuter(){
  outerSizeX = (Attachment55x55x35_numHolesX * holeSpacing) + wallThickness;
  outerSizeY = (Attachment55x55x35_numHolesY * holeSpacing) + wallThickness;
  outerSizeZ = (Attachment55x55x35_numHolesZ * holeSpacing) + wallThickness;
  cube([outerSizeX, outerSizeY, outerSizeZ], center=true);
}

// Generate the inner cutout of the part.
// It has to leave the bottom closed and the top open.
module Attachment55x55x35_BoxInner(){ 
  innerSizeX = (Attachment55x55x35_numHolesX * holeSpacing) - wallThickness;
  innerSizeY = (Attachment55x55x35_numHolesY * holeSpacing) - wallThickness;
  innerSizeZ = (Attachment55x55x35_numHolesZ * holeSpacing) + wallThickness;
  translate([0, 0, wallThickness])
    cube([innerSizeX, innerSizeY, innerSizeZ], center=true);
}

// Generate drill tools for cutting along X axis
module Attachment55x55x35_DrillX(){
  drillXTranslateX = -wallThickness;
  drillXTranslateY = -(Attachment55x55x35_numHolesY * holeSpacing) / 2 + holeSpacing / 2;
  drillXTranslateZ = -(Attachment55x55x35_numHolesZ * holeSpacing) / 2 + holeSpacing / 2;
  translate([drillXTranslateX, drillXTranslateY, drillXTranslateZ])
  for(xy = [0 : Attachment55x55x35_numHolesY - 1]){
    for(xz = [0 : Attachment55x55x35_numHolesZ - 1]){
      translate([0, xy * holeSpacing, xz * holeSpacing])
        rotate([0, 90, 0])
          cylinder(h = wallThickness * 2, d = holeDiameter, $fn = holeFN);
    }
  }
}

module Attachment55x55x35_DrillY(){
  drillYTranslateX = -(Attachment55x55x35_numHolesX * holeSpacing) / 2 + (holeSpacing / 2);
  drillYTranslateY = wallThickness;
  drillYTranslateZ = -(Attachment55x55x35_numHolesZ * holeSpacing) / 2 + (holeSpacing / 2);
  translate([drillYTranslateX, drillYTranslateY, drillYTranslateZ])
  for(xx = [0 : Attachment55x55x35_numHolesX - 1]){
    for(xz = [0 : Attachment55x55x35_numHolesZ - 1]){
      translate([xx * holeSpacing, 0, xz * holeSpacing])
        rotate([90, 0, 0])
          cylinder(h = wallThickness * 2, d = holeDiameter, $fn = holeFN);
    }
  }
}

module Attachment55x55x35_DrillZ(){
  drillZTranslateX = -(Attachment55x55x35_numHolesX * holeSpacing) / 2 + (holeSpacing / 2);
  drillZTranslateY = -(Attachment55x55x35_numHolesY * holeSpacing) / 2 + (holeSpacing / 2);
  drillZTranslateZ = -wallThickness;
  translate([drillZTranslateX, drillZTranslateY, drillZTranslateZ])
  for(xx = [0 : Attachment55x55x35_numHolesX - 1]){
    for(xy = [0 : Attachment55x55x35_numHolesY - 1]){
      translate([xx * holeSpacing, xy * holeSpacing, 0])
        rotate([0, 0, 90])
          cylinder(h = wallThickness * 2, d = holeDiameter, $fn = holeFN);
    }
  }
}

Attachment55x55x35();
