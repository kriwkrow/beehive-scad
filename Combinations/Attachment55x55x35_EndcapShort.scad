include <../Variables.scad>

use <../Parts/Attachment55x55x35.scad>
use <../Parts/EndcapShort.scad>

module Attachment55x55x35_EndcapShort(){
  union(){
    Attachment55x55x35();
    
    endcapTranslateX = 
      (holeSpacing * Attachment55x55x35_numHolesX) / 2 +
      Beam_headLength / 2 + wallThickness;
    endcapTranslateZ = (((Attachment55x55x35_numHolesZ * holeSpacing) + (wallThickness)) - (Beam_size + wallThickness)) / 2;
    translate([endcapTranslateX, 0, -endcapTranslateZ])
      EndcapShort();
  }
}

Attachment55x55x35_EndcapShort();